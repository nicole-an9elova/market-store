// ignore: unused_import
import 'package:market_store/market_store.dart' as market_store;

enum Type { Bronze, Silver, Gold }

abstract class TypeOfCards {
  String owner;
  double turnover;
  double purchaseValue;

  set theOwner(String newOwner) {
    owner = newOwner ?? owner;
  }

  set theTurnover(double newTurnover) {
    turnover = newTurnover ?? turnover;
  }

  set thePurchaseValue(double newPurchValue) {
    purchaseValue = newPurchValue ?? purchaseValue;
  }

  String get theOwner => owner;
  double get theTurnover => turnover;
  double get thePurchaseValue => purchaseValue;

  TypeOfCards(this.turnover, this.purchaseValue);

  double calcInitDiscRate();
}

class Bronze extends TypeOfCards {
  Bronze(double turnover, double purchaseValue)
      : super(turnover, purchaseValue);

  @override
  double calcInitDiscRate() {
    var tempDiscRate = 0.0;
    if (theTurnover >= 100 && theTurnover <= 300) {
      tempDiscRate = 1;
    } else if (theTurnover > 300) {
      tempDiscRate = 2.5;
    }
    return tempDiscRate;
  }
}

class Silver extends TypeOfCards {
  Silver(double turnover, double purchValue) : super(turnover, purchValue);
  @override
  double calcInitDiscRate() {
    var tempDiscRate = 0.0;
    theTurnover > 300 ? tempDiscRate = 3.5 : tempDiscRate = 2.0;
    return tempDiscRate;
  }
}

class Gold extends TypeOfCards {
  Gold(double turnover, double purchValue) : super(turnover, purchValue);
  @override
  double calcInitDiscRate() {
    var tempDiscRate = 2.0;
    var tempTurnover = theTurnover;
    while (tempTurnover >= 100) {
      tempDiscRate++;
      tempTurnover -= 100;
    }
    return tempDiscRate >= 10 ? tempDiscRate = 10 : tempDiscRate;
  }
}

class PayDesk {
  static double initDiscRate(TypeOfCards card) {
    return card.calcInitDiscRate();
  }

  static double calcDiscount(TypeOfCards card) {
    var discRate = card.calcInitDiscRate() / 100;
    return discRate * card.thePurchaseValue;
  }

  static double total(TypeOfCards card) {
    return card.thePurchaseValue - calcDiscount(card);
  }

  void printCard(TypeOfCards card) {
    print('Purchase value: \$${card.thePurchaseValue}');
    print('Discount rate: ${initDiscRate(card)}%');
    print('Discount: \$${calcDiscount(card).toStringAsFixed(2)}');
    print('Total: \$${total(card)}');
  }
}

void main() {
  var bronze = Bronze(0, 150);
  var silver = Silver(600, 850);
  var gold = Gold(1500, 1300);
  var desk = PayDesk();
  desk.printCard(bronze);
  desk.printCard(silver);
  desk.printCard(gold);
}
